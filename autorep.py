import os


def just_change(new, old):
    # getting the current repository url
    os.system("echo " + "==========Getting new remote url==========")
    current = os.popen("git remote get-url --push origin").read()
    # removing 'git@bitbucket.org' from the repo
    current_final = current[len(old):-1]

    # new url
    # placing the new url infront of the location
    command = f'''git remote set-url origin {new}{current_final}'''
    os.system(command)
    os.system("echo " + "==========Created new url==========")
    os.system("echo " + "Current url==" + new + current_final)


# options : git@bitbucket.org:redkiwi-wp/ , git@bitbucket.org:redkiwi-general/ , git@bitbucket.org:redkiwi/ , git@bitbucket.org:redkiwi-sites/ ,
just_change("git@bitbucket.org:redkiwi-general/", "https://git.redkiwi.nl/")